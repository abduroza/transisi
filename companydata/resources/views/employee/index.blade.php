@extends('layouts.master')

@section('content')
    @if(session('success'))
    <div class="alert alert-success" role="alert">
        {{session('success')}}
    </div>
    @endif
    @if(session('errors'))
    <div class="alert alert-danger" role="alert">
        {{session('errors')}}
    </div>
    @endif
    <div class="row">
        <div class="col-6">
            <h1>Data Employee</h1>
        </div>
        <div class="col-6">
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary float-right btn-sm" data-toggle="modal" data-target="#exampleModal">
                Tambah Data
            </button>
        </div>
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama Employee</th>
                    <th scope="col">Company</th>
                    <th scope="col">Email</th>
                    <th scope="col">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $no_urut = 1;
                @endphp
                @foreach ($data_employee as $employee)
                <tr>
                    <th scope="row">{{$no_urut}}</th>
                    <td>{{$employee->nama}}</td>
                    <td>{{$employee->company->nama}}</td>
                    <td>{{$employee->email}}</td>
                <td>
                    <a href="/employee/{{$employee->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    <form action="/employee/{{$employee->id}}" method="post">
                        {{csrf_field()}}
                        {{ method_field('delete') }}
                        <button class="btn btn-danger btn-sm" type="submit" onClick="return confirm('Yakin mau dihapus')">Hapus</button>
                    </form>
                </td>
                </tr>
                @php
                    $no_urut ++;
                @endphp
                @endforeach
            </tbody>
        </table>
        <div>
            {{$data_employee->links()}}
        </div>  
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Data Employee</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="/employee" method="POST">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="inputNama">Nama Employee</label>
                            <input name="nama" type="text" class="form-control" id="inputNama" aria-describedby="emailHelp" required>
                            @if($errors->has('nama'))
                                <span class="help-block alert-danger">{{$errors->first('nama')}}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Company</label>
                            <select class="form-control" id="exampleFormControlSelect1" name="company_id">
                                @foreach ($data_company as $company)
                                    <option value="{{$company->id}}">{{$company->nama}}</option>
                                @endforeach
                            </select>
                            @if($errors->has('company_id'))
                                <span class="help-block alert-danger">{{$errors->first('company_id')}}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email</label>
                            <input name="email" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required>
                            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                            @if($errors->has('email'))
                                <span class="help-block alert-danger">{{$errors->first('email')}}</span>
                            @endif
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
