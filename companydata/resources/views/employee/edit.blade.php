@extends('layouts.master')

@section('content')
    @if(session('success'))
    <div class="alert alert-success" role="alert">
        {{session('success')}}
    </div>
    @endif
    <div class="row">
        <H1>Edit Data Employee</H1>
        <div class="col-lg-12">
            <form action="/employee/{{$data_employee->id}}" method="post">
                {{csrf_field()}}
                {{ method_field('PUT') }}
                <div class="form-group">
                    <label for="inputNama">Nama Employee</label>
                    <input name="nama" type="text" class="form-control" id="inputNama" aria-describedby="emailHelp" placeholder="Nama company..." required value="{{$data_employee->nama}}">
                    @if($errors->has('nama'))
                        <span class="help-block alert-danger">{{$errors->first('nama')}}</span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Company</label>
                    <select class="form-control" id="exampleFormControlSelect1" name="company_id">
                        @foreach ($data_company as $company)
                            <option value="{{$company->id }}" {{ $data_employee->company_id == $company->id ? 'selected' : '' }}>{{$company->nama}}</option>
                        @endforeach
                    </select>
                    @if($errors->has('company_id'))
                        <span class="help-block alert-danger">{{$errors->first('company_id')}}</span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Email</label>
                    <input name="email" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required value="{{$data_employee->email}}">
                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                    @if($errors->has('email'))
                        <span class="help-block alert-danger">{{$errors->first('email')}}</span>
                    @endif
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection
