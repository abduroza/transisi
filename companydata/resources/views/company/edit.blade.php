@extends('layouts.master')

@section('content')
    @if(session('success'))
    <div class="alert alert-success" role="alert">
        {{session('success')}}
    </div>
    @endif
    <div class="row">
        <H1>Edit Data Company</H1>
        <div class="col-lg-12">
            <form action="/company/{{$data_company->id}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                {{ method_field('PUT') }}
                <div class="form-group">
                    <label for="inputNama">Nama Company</label>
                    <input name="nama" type="text" class="form-control" id="inputNama" aria-describedby="emailHelp" placeholder="Nama company..." required value="{{$data_company->nama}}">
                    @if($errors->has('nama'))
                        <span class="help-block alert-danger">{{$errors->first('nama')}}</span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Email</label>
                    <input name="email" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required value="{{$data_company->email}}">
                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                    @if($errors->has('email'))
                        <span class="help-block alert-danger">{{$errors->first('email')}}</span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="inputWeb">Website</label>
                    <input name="website" type="text" class="form-control" id="inpuWeb" aria-describedby="emailHelp" required value="{{$data_company->website}}">
                    @if($errors->has('website'))
                        <span class="help-block alert-danger">{{$errors->first('website')}}</span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="inputLogo">Logo</label>
                    <input name="logo" type="file" class="form-control-file" id="inputLogo">
                    <small class="form-text text-muted">Foto berformat jpg, jpeg atau png</small>
                    @if($errors->has('logo'))
                        <span class="help-block alert-danger">{{$errors->first('logo')}}</span>
                    @endif
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection
