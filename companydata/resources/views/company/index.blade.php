@extends('layouts.master')

@section('content')
    @if(session('success'))
    <div class="alert alert-success" role="alert">
        {{session('success')}}
    </div>
    @endif
    @if(session('errors'))
    <div class="alert alert-danger" role="alert">
        {{session('errors')}}
    </div>
    @endif
    <div class="row">
        <div class="col-6">
            <h1>Data Company</h1>
        </div>
        <div class="col-6">
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary float-right btn-sm" data-toggle="modal" data-target="#exampleModal">
                Tambah Data
            </button>
        </div>
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama Company</th>
                    <th scope="col">Email</th>
                    <th scope="col">Logo</th>
                    <th scope="col">Website</th>
                    <th scope="col">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $no_urut = 1;
                @endphp
                @foreach ($data_company as $company)
                <tr>
                    <th scope="row">{{$no_urut}}</th>
                    <td>{{$company->nama}}</td>
                    <td>{{$company->email}}</td>
                    <td>
                        <img src="/storage/company/{{$company->logo}}" width="50" height="50" alt="{{$company->nama}}">
                    </td>
                    <td>{{$company->website}}</td>
                <td>
                    <a href="/company/{{$company->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    <form action="/company/{{$company->id}}" method="post">
                        {{csrf_field()}}
                        {{ method_field('delete') }}
                        <button class="btn btn-danger btn-sm" type="submit" onClick="return confirm('Yakin mau dihapus')">Hapus</button>
                    </form>
                </td>
                </tr>
                @php
                    $no_urut ++;
                @endphp
                @endforeach
            </tbody>
        </table>
        <div>
            {{$data_company->links()}}
        </div>  
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Data Company</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="/company" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="inputNama">Nama Company</label>
                            <input name="nama" type="text" class="form-control" id="inputNama" aria-describedby="emailHelp" placeholder="Nama company..." required>
                            @if($errors->has('nama'))
                                <span class="help-block alert-danger">{{$errors->first('nama')}}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email</label>
                            <input name="email" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required>
                            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                            @if($errors->has('email'))
                                <span class="help-block alert-danger">{{$errors->first('email')}}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="inputWeb">Website</label>
                            <input name="website" type="text" class="form-control" id="inputWeb" aria-describedby="emailHelp" required>
                            @if($errors->has('website'))
                                <span class="help-block alert-danger">{{$errors->first('website')}}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="inputLogo">Logo</label>
                            <input name="logo" type="file" class="form-control-file" id="inputLogo">
                            <small class="form-text text-muted">Foto berformat jpg, jpeg atau png</small>
                            @if($errors->has('logo'))
                                <span class="help-block alert-danger">{{$errors->first('logo')}}</span>
                            @endif
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
