<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use DB;
use File;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::paginate(5);

        return view('company.index', ['data_company' => $companies]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validasiStore($request);

        DB::beginTransaction();
        try {
            $name_logo = NULL;
            if($request->hasFile('logo')) {
                $file = $request->file('logo');
                $name_logo = $request->email . '-' . time() . '.' . $file->getClientOriginalExtension();
                $file->storeAs('public/company', $name_logo);
            }
    
            $company = Company::create([
                'nama' => $request->nama,
                'email' => $request->email,
                'website' => $request->website,
                'logo' => $name_logo
            ]);
    
            DB::commit();
            return redirect('/company')->with('success', 'Berhasil menambahkan company baru');   
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect('/company')->with('errors', 'Gagal menambahkan company baru');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::find($id);

        return view('company.edit', ['data_company' => $company]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validasiUpdate($request);

        $company = Company::find($id);
        $name_logo = $company->logo;

        if($request->hasFile('logo')){
            $file = $request->file('logo');
            File::delete(storage_path('app/public/company/' . $name_logo));
            $name_logo = $request->email . '-' . time() . '.' . $file->getClientOriginalExtension();
            $file->storeAs('public/company', $name_logo);
        }
        $company->update([
            'nama' => $request->nama,
            'email' => $request->email,
            'logo' => $name_logo,
            'website' => $request->website
        ]);
        return redirect('/company')->with('success', 'Berhasil mengupdate company '.$company->nama);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = Company::find($id);
        File::delete(storage_path('app/public/company/' . $company->logo));
        $company->delete();

        return redirect('/company')->with('success', 'Berhasil menghapus data company');
    }

    private function validasiStore($request)
    {
        return $this->validate($request, [
            'nama' => 'required|string|min:3',
            'email' => 'required|email|unique:companies,email',
            'logo' => 'required|image|mimes:jpg,png,jpeg|dimensions:min_width=100,min_height=100|max:20280',
            'website' => 'required'
        ]);
    }

    private function validasiUpdate($request)
    {
        return $this->validate($request, [
            'nama' => 'required|string|min:3',
            'email' => 'required|email',
            'logo' => 'image|mimes:jpg,png,jpeg|dimensions:min_width=100,min_height=100|max:20280',
            'website' => 'required'
        ]);
    }
}
