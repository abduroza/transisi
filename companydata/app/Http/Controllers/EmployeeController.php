<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\Company;
use DB;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employee = Employee::paginate(5);
        $company = Company::all();

        return view('employee.index', ['data_employee' => $employee, 'data_company' => $company]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validasiStore($request);

        DB::beginTransaction();
        try {
            $employee = Employee::create([
                'nama' => $request->nama,
                'company_id' => $request->company_id,
                'email' => $request->email,
            ]);
            DB::commit();
            return redirect('/employee')->with('success', 'Berhasil menambahkan employee baru');   
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect('/employee')->with('errors', 'Gagal menambahkan employee baru');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = Employee::find($id);
        $company = Company::all();
        return view('employee.edit', ['data_employee' => $employee, 'data_company' => $company]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validasiUpdate($request);
        $employee = Employee::find($id);

        $employee->update([
            'nama' => $request->nama,
            'company_id' => $request->company_id,
            'email' => $request->email,
        ]);

        return redirect('/employee')->with('success', 'Berhasil mengupdate data employee');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee = Employee::find($id);
        $employee->delete();

        return redirect('/employee')->with('success', 'Berhasil menghapus data employee');
    }

    private function validasiStore($request)
    {
        return $this->validate($request, [
            'nama' => 'required|min:3',
            'email' => 'required',
            'company_id' => 'required|exists:companies,id',
        ]);
    }

    private function validasiUpdate($request)
    {
        return $this->validate($request, [
            'nama' => 'required|min:3',
            'email' => 'required',
            'company_id' => 'required',
        ]);
    }
}
