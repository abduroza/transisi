<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UsesUuid;

class Employee extends Model
{
    use UsesUuid;

    protected $guarded = [];

    public function company(){
        return $this->belongsTo(Company::class);
    }
}
