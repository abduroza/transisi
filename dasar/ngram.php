<?php

function ngram($uni){
    $inputan = str_split($uni);
    $panjang = count($inputan);

    $unigram = str_replace(' ', ', ', $uni);

    $pjgSpace = null;
    $bigram = null;
    $trigram = null;
    for($i = 0; $i < $panjang; $i++){
        if($inputan[$i] != " "){
            $ada[] = $inputan[$i];
            $bigram[] = $inputan[$i];
            $trigram[] = $inputan[$i];
        } else {
            $pjgSpace++;
            if($pjgSpace == 2 || $pjgSpace == 4 || $pjgSpace == 6 || $pjgSpace == 8|| $pjgSpace == 10){
                $bigram[] = ", ";
                $trigram[] = " ";
            } else {
                if($pjgSpace == 3 || $pjgSpace == 6 || $pjgSpace == 9 || $pjgSpace == 12){
                    $trigram[] = ", ";
                } else {
                    $trigram[] = " ";
                }
                $bigram[] = " ";
            }
        }
    }

    echo "Unigram: " . $unigram;
    echo '<br>';
    echo "Bigram: " . (implode("", $bigram));
    echo '<br>';
    echo "Trigram: " . (implode("", $trigram));
}

ngram("Jakarta adalah ibukota negara Republik Indonesia");
