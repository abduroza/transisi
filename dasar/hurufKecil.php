<?php

function huruf($input){
    $inputan = str_split($input);
    
    $hurufKecil = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];

    $jumlahAbjad = count($hurufKecil);
    $jumlahInput = count($inputan);

    $inputKecil = null;
    for($x = 0; $x <= $jumlahAbjad; $x++) {
        for($y = 0; $y < $jumlahInput; $y++) {
            if($hurufKecil[$x] == $inputan[$y]){
                $inputKecil++;
            }
        }
    }
    echo $input . " mengandung " .$inputKecil. " buah huruf kecil";
}
huruf("TranSISI");
